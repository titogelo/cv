$(document).ready(function(){

	var duracion = 9; // en segundos
	$("header nav ul li a:first").addClass("activo");


	// /////////////////////////////////////////////////////////////////
	// Tooltip en el boton
	// /////////////////////////////////////////////////////////////////

	$("#profile article.social a.qr img").tooltip({
			position: {
                my: "top top",
                at: "top top",

                using: function( position, feedback ) {
                    $( this ).css( position );
                    $( "<div>" )
                        .addClass( "arrow" )
                        .addClass( feedback.vertical )
                        .addClass( feedback.horizontal )
                        .appendTo( this );
                }
            }
	});

	// /////////////////////////////////////////////////////////////////
	// Tooltip en el imagen
	// /////////////////////////////////////////////////////////////////

	$("#home article section div").tooltip();

	// /////////////////////////////////////////////////////////////////
	// Cambiar de idioma
	// /////////////////////////////////////////////////////////////////

	$("header .languages a").click(function(e){
		$(this).toggleClass("activo");
	});

	// /////////////////////////////////////////////////////////////////
	// Cambiar de sección manualmente
	// /////////////////////////////////////////////////////////////////

	$("header nav ul li a").click(function(){
		var $anchor = $(this);

		$("header nav ul li a").removeClass("activo");
		$anchor.addClass("activo");


		$('html, body').animate({
			scrollLeft: $($anchor.attr('id')).offset().left
		}, 1000, /*'easeInOutBack',*/function(){


			if($anchor.attr('id') == "#work"){
				$("nav").addClass("contraste");
			}
			else{
				$("nav").removeClass("contraste");
			}
		});

	});

	// /////////////////////////////////////////////////////////////////
	// Tour por el CV 
	// /////////////////////////////////////////////////////////////////


	$("#home article section div").click(function() {
		var timeout = 0;

		//$("#home article section div.contador").show();
		//$("header div.contador").show();

    	jQuery.each($("header nav ul li a"), function() {
	    	var aux = $(this).attr('id');

			setTimeout(function() {
				$('html, body').stop().animate({
					scrollLeft: $(aux).offset().left
				}, 2000);

				var c = duracion; 
				var intervalID = setInterval(function(){
				    if (c>0){
				    	//$("#home article section div").removeClass("numero"+(c));
				    	//$("header div").removeClass("numero"+(c));
				    	$("header div").fadeOut(0, function(){
        					$(this).empty().append(c).fadeIn();
   						});
				    	c--;
				        //$("#home article section div").addClass("numero"+(c))
				        //$("header div").addClass("numero"+(c))
				        //$("header div").append(c);
				    } else {
				        clearInterval(intervalID);
				    }
				}, 1000);

				console.log("Cambio de seccion");

        	}, timeout);

    		timeout += duracion*1000;
    	
    	});
	});

	// /////////////////////////////////////////////////////////////////
	// Redimensionado del navegador utilizando el plugin smartresize
	// /////////////////////////////////////////////////////////////////

	$(window).smartresize(function(){
		if($("header nav ul li a").hasClass("activo")){
			var $anchor = $("header nav ul li a.activo");

			$('html, body').animate({
				scrollLeft: $($anchor.attr('id')).offset().left
			},1000);
		}
	});

	// /////////////////////////////////////////////////////////////////
	// Utilización del API de twitter
	// /////////////////////////////////////////////////////////////////

    var user = 'titogelo';

    var url = 'https://api.twitter.com/1/statuses/user_timeline.json?include_entities=true&include_rts=true&screen_name='+user+'&count=5';

	$.getJSON(url+"&callback=?", function(data){
    	$.each(data, function(indice, valor){

    		tweet = valor.text;

    		tweet = tweet.replace(/(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig, function(url) {
	            return '<a href="'+url+'">'+url+'</a>';
	        }).replace(/B@([_a-z0-9]+)/ig, function(reply) {
	            return  reply.charAt(0)+'<a href="http://twitter.com/'+reply.substring(1)+'">'+reply.substring(1)+'</a>';
	        });
	      
	        $("section#profile article.twitter div").append("<p>"+tweet+"</p>");			
    	});
    });

	// /////////////////////////////////////////////////////////////////
	// Función que se utiliza para la internacionalización
	// /////////////////////////////////////////////////////////////////

	function loaded(){
		var lang = getParameterValue("lang");
		if (lang != ""){
			String.locale = lang;
		}

		document.title = _(document.title);

		localizeHTMLTag("subtitle");
		localizeHTMLTag("#home");
		localizeHTMLTag("#profile");
		localizeHTMLTag("#contact");
		localizeHTMLTag("profile_social");
		localizeHTMLTag("long_profile_descrition");
		localizeHTMLTag("social_social");
		localizeHTMLTag("social_twitter");
		localizeHTMLTag("contact_contact");
	}

	var _ = function (string) {
		return string.toLocaleString();
	};

	function localizeHTMLTag(tagId)
	{
		tag = document.getElementById(tagId);
		tag.innerHTML = _(tag.innerHTML);
	}

	function getParameterValue(parameter){
		parameter = parameter.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
		var regexS = "[\\?&]" + parameter + "=([^&#]*)";
		var regex = new RegExp(regexS);
		var results = regex.exec(window.location.href);
		if(results == null)
			return "";
		else
			return results[1];
	}

	// /////////////////////////////////////////////////////////////////
	// Invocacion al metodo de internacionalización
	// /////////////////////////////////////////////////////////////////

	loaded();

});



